package indglobal.com.softsoon;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Image extends Fragment {


    RecyclerView recyclerView;
    String[] titlename = {"a","b","c","d"};
    public ArrayList<Integer> IMAGES = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_image);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        IMAGES.add(R.drawable.singer1);
        IMAGES.add(R.drawable.singer2);
        IMAGES.add(R.drawable.singer3);
        IMAGES.add(R.drawable.singer4);
        IMAGES.add(R.drawable.singer5);
        Image_Adapter image_adapter = new Image_Adapter(getActivity(),titlename,IMAGES);
        recyclerView.setAdapter(image_adapter);
        return view;
    }

}
