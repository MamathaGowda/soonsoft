package indglobal.com.softsoon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by indglobal on 6/22/2017.
 */

public class Image_Adapter extends RecyclerView.Adapter<Image_Adapter.ViewHolder> {

    Context context;
    String[] name;
    LayoutInflater inflater;
    Image_Adapter.ViewHolder viewHolder1;
    ArrayList<Integer> IMAGES;

    public Image_Adapter(Context context, String[] name,ArrayList<Integer> IMAGES)
    {
        this.context = context;
        this.name = name;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public  Image_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_image, parent, false);
        viewHolder1 = new Image_Adapter.ViewHolder(v);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.img_images.setBackgroundResource(IMAGES.get(position));
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_images;

        public ViewHolder(View itemView) {
            super(itemView);
            img_images = (ImageView) itemView.findViewById(R.id.img_images);

        }
    }
}
