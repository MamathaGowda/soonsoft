package indglobal.com.softsoon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by indglobal on 6/22/2017.
 */

public class Video_Adapter extends RecyclerView.Adapter<Video_Adapter.ViewHolder> {

    Context context;
    String[] name;
    ArrayList<Integer> IMAGES;
    LayoutInflater inflater;
    Video_Adapter.ViewHolder viewHolder1;

    public Video_Adapter(Context context,String[] name,ArrayList<Integer> IMAGES)
    {
        this.context = context;
        this.name = name;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public  Video_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_video, parent, false);
        viewHolder1 = new Video_Adapter.ViewHolder(v);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.rl_video.setBackgroundResource(IMAGES.get(position));
        holder.tv_title1.setText(name[position]);
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title1;
        RelativeLayout rl_video;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title1 = (TextView) itemView.findViewById(R.id.tv_title1);
            rl_video = (RelativeLayout) itemView.findViewById(R.id.rl_video);

        }
    }
}
