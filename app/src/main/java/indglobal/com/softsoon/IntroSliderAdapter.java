package indglobal.com.softsoon;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Android on 8/4/16.
 */
public class IntroSliderAdapter extends PagerAdapter {

    private ArrayList<Integer> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    int returnCount;


    public IntroSliderAdapter(Context context, ArrayList<Integer> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {

        int df =IMAGES.size();
        Double d = Math.sqrt(df);
        returnCount = d.intValue();
        return returnCount;


    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.intro_sliding_layout, view, false);

        assert imageLayout != null;
       // final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.ivSliderImage);

        final LinearLayout ll = (LinearLayout) imageLayout.findViewById(R.id.ll);
        final TextView title = (TextView)imageLayout.findViewById(R.id.tvsliderTitle);
        final TextView subtitle = (TextView)imageLayout.findViewById(R.id.tvsliderSubTitl);

        String img = IMAGES.get(position) + "";
        img = img.replace("[", "").replace("]", "");

//        Picasso.with(context).load(img).error(R.drawable.about_why).into(imageView);

  //      imageView.setImageDrawable(context.getResources().getDrawable(IMAGES.get(position)));
        ll.setBackgroundResource(IMAGES.get(position));
        title.setText(MainActivity.IntroTitle.get(position));
        subtitle.setText(MainActivity.IntroSubTitle.get(position));

        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}

