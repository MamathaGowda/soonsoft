package indglobal.com.softsoon;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Video extends Fragment {

RecyclerView recyclerView;
String[] titlename = {"Pallivaalu Bhadravattakam","G-Eazy x Bebe Rexha","When It's Dark Out","Kerala Boat Song","Ed Sheeran - Shape of You"};
    public ArrayList<Integer> IMAGES = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_video);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        IMAGES.add(R.drawable.vidya);
        IMAGES.add(R.drawable.easy);
        IMAGES.add(R.drawable.dark);
        IMAGES.add(R.drawable.kerla);
        IMAGES.add(R.drawable.shape);
        Video_Adapter video_adapter = new Video_Adapter(getActivity(),titlename,IMAGES);
        recyclerView.setAdapter(video_adapter);
        return view;

    }
}
